#Codigo realizado para la etapa de tecnical challenges de atomic mind

'''Funcion realizada para conocer los coheficientes de un polinomio grado n
utilizando el triangulo de pascal'''           
            
def pascal(n):
    
    array1=[];
    array2=[];
    resultado=[];
    i=0;
    m=1;
    j=0;

    while j <= n:
        
        for i in range(m):
            if i==0:
                a=n-(n-1);
                array1.append(a);
                i=i+1;
                
            elif i==m-1:
                a=n-(n-1);
                array1.append(a);
                i=i+1;
                
            else:
                a=(array2[i-1] + array2[i]);
                array1.append(a);
                i=i+1;
                
        array2=array1;
        array1=[];
        j=j+1;
        m=m+1;
        
    resultado=array2;
    return resultado; 
