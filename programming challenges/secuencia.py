#Codigo realizado para la etapa de tecnical challenges de atomic mind

'''programa que genera una secuencia siguiendo el metodo dela sucesion de fibonacci'''

def valores():
    d=input("ingrese el numero de valores a generar");
    a=input("ingrese el primer valor de la secuencia");
    b=input("ingrese el segundo valor de la secuencia");
    if b <= a:
        print ("el primer valor de la secuencia debe ser menor al segundo");
        print("\n")
        valores();
    return d,a,b;

def secuencia():
    array=[];
    val=valores();
    i=0;
    
    for i in range (val[0]):
        
        if i==0 :
            array.append(val[i+1]+val[i+2]);
        elif i==1:
            array.append(val[i+1]+array[i-1]);
        else:
            array.append(array[i-1]+array[i-2]);
    respuesta=array;
    return respuesta;