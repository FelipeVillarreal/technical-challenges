# programa realizado para codificar o decodificar una cadena de strings
# utilizando el metodo Caesar Shift Cipher
def menu():
    print ("escoja una de las siguientes opciones: ")
    print ("\t1 - Codificar un mensaje")
    print ("\t2 - Decodificar un mensaje")
    
    a=input();
    
    proceso1(a);
    
    print ("Escoja una de las siguientes opciones: ")
    print ("\t1 - volver al menu principal")
    print ("\t2 - salir")
    
    b=input();
    
    proceso2(b);

def proceso1(a):
    if a==1:
        cadena = raw_input("Introduce la frase a codificar: ");
        respuesta=codificar(cadena);
        print ("La frase codificada es: ");
        print respuesta
    elif a==2:
        cadena = raw_input("Introduce la frase a decodificar: ");
        respuesta=decodificar(cadena);
        print ("La frase decodificada es: ");
        print respuesta
    else:
        print ("ingrese una opcion valida.")
        menu();
    return respuesta;

def proceso2(b):
    if b==1:
        menu();

def codificar(cadena):
    #cadena = raw_input("Introduce la frase a codificar: ");
    listastr1 = list(cadena);
    listanum1 = [];
    listanum2 = [];
    listastr2 = [];
    n=len(listastr1);
    i=0;

    for i in range (n):
        num=ord(listastr1[i]);
        if num >= 65 and num <= 90:
            listanum1.append(num);
        elif num>=97 and num <=122:
            listanum1.append(num);
        else:
            num=32;
            listanum1.append(num);
        i+=i;

    for i in range (n):
        if listanum1[i] >= 65 and listanum1[i] <=87:
            a=listanum1[i]+3;
            listanum2.append(a);
        elif listanum1[i]>=97 and listanum1[i] <=119:
            a=listanum1[i]+3;
            listanum2.append(a);
        elif listanum1[i] >= 88 and listanum1[i] <=90:
            a=listanum1[i]-23;
            listanum2.append(a);
        elif listanum1[i] >= 120 and listanum1[i] <=122:
            a=listanum1[i]-23;
            listanum2.append(a);
        else:
            a=32;
            listanum2.append(a);
        i+=i;
    
    for i in range(n):
        b=chr(listanum2[i]);
        listastr2.append(b);
    
    codigo="".join(listastr2);
    return codigo;

def decodificar(cadena):
    cadena = raw_input("Introduce la frase a decodificar: ");
    listastr1 = list(cadena);
    listanum1 = [];
    listanum2 = [];
    listastr2 = [];
    n=len(listastr1);
    i=0;

    for i in range (n):
        num=ord(listastr1[i]);
        if num >= 65 and num <= 90:
            listanum1.append(num);
        elif num>=97 and num <=122:
            listanum1.append(num);
        else:
            num=32;
            listanum1.append(num);
        i+=i;

    for i in range (n):
        if listanum1[i] >= 68 and listanum1[i] <=90:
            a=listanum1[i]-3;
            listanum2.append(a);
        elif listanum1[i]>=100 and listanum1[i] <=122:
            a=listanum1[i]-3;
            listanum2.append(a);
        elif listanum1[i] >= 65 and listanum1[i] <=67:
            a=listanum1[i]+23;
            listanum2.append(a);
        elif listanum1[i] >= 97 and listanum1[i] <=99:
            a=listanum1[i]+23;
            listanum2.append(a);
        else:
            a=32;
            listanum2.append(a);
        i+=i;
    
    for i in range(n):
        b=chr(listanum2[i]);
        listastr2.append(b);
    
    codigo="".join(listastr2);
    return codigo;
